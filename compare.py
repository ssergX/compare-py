def compare(start_string):
    number_first, number_two = int(start_string[0]), int(start_string[2])
    if start_string[1] in ">":
        return number_first > number_two
    return number_first < number_two

